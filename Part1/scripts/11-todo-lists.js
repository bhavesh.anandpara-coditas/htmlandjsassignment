const todoList = [
    {
        name : 'make dinner',
        dueDate : '2022-12-22'
    },
    {
        name : 'wash dishes',
        dueDate : '2022-12-22'
    }
];

function render(){
    let todoListHTML = '';
    
    for( let i = 0 ; i < todoList.length ; i++ ){
    
        const todoObj = todoList[i];
        const {name,dueDate} = todoObj

        const html = `
        <div> ${todo} </div>
        <div> ${dueDate} </div>
        <button onclick="
            todoList.splice(${i}, 1);
            render();
        " >Delete</button>
        </div>
        `
        todoListHTML += html;
    
    }
    
    document.querySelector('.js-todo-list').innerHTML = todoListHTML;

}


function addToDo(){
    const inputElement = document.querySelector('.js-name-input')
    const dateInputElement = document.querySelector('.js-due-date-input')

    const name = inputElement.value
    const dueDate = dateInputElement.value

    todoList.push({name,dueDate})

    inputElement.value = '';
    render();
}